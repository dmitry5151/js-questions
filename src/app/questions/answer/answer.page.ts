import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
    selector: 'app-answer',
    templateUrl: './answer.page.html',
    styleUrls: ['./answer.page.scss'],
})
export class AnswerPage implements OnInit {

    public item: any;

    constructor(
        private router: Router,
        private route: ActivatedRoute,
        // private
    ) { }

    ngOnInit() {
        this.item = history?.state?.item;
    }

}
