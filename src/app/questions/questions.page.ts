import { Component, OnInit } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { ActivatedRoute, Router } from '@angular/router';

import { ApiService } from '../api/api.service';

@Component({
    selector: 'app-questions',
    templateUrl: './questions.page.html',
    styleUrls: ['./questions.page.scss'],
})
export class QuestionsPage implements OnInit {
    public questionsCategory: string;
    public data: any;
    public questions: any;
    public view: 'list' | 'single' = 'single';
    public currentQuestion: any;
    public viewAnswer = false;
    private questionsSequence: Iterator<any>;

    constructor(
        private api: ApiService,
        private firestore: AngularFirestore,
        private router: Router,
        private route: ActivatedRoute,
    ) { }

    ngOnInit() {
        this.data = this.firestore.collection('questions-test').valueChanges();
        this.questionsCategory = this.route.snapshot.paramMap.get('id');

        this.api.getQuestions(this.questionsCategory).subscribe(q => {
            this.questions = q;
            this.questionsSequence = this.generateSequence(q as any[]);
            this.nextQuestion();
        });
    }

    showAnswer(item: any): void {
        this.router.navigate(['answer'], { relativeTo: this.route, state: { item } });
    }

    toggleView(view: 'list' | 'single'): void {
        this.view = view;
    }

    nextQuestion() {
        this.currentQuestion = this.questionsSequence.next().value;
    }

    repeat(): void {
        this.questionsSequence = this.generateSequence(this.questions as any[]);
        this.currentQuestion = this.questionsSequence.next().value;
    }


    private *generateSequence(questions: any[]): Iterator<any> {
        for (const question of questions) {
            yield question;
        }
    }

}
