import { HttpClient, HttpClientModule, HttpHandler } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class HttpService extends HttpClient {

    constructor(handler: HttpHandler) {
        super(handler);
    }

    /**
     * Здесь можно добавить вызов лоудера или выполнить любую другую логику
     * Такое решение имеет смысл, если на любой http-запрос будет требоваться
     * показывать загрузчик (спиннер или скелетон)
     * Декоратор же позволяет оснащать загрузчиком только выборочные запросы
     * Если на странице более одного запроса для получения комплекса данных,
     * запросы можно вложить в один объединяющий метод, декорировав его @loading().
     * В этом случае загрузчик отключится после того, как выполнятся все запросы.
     * @param url
     * @param options
     * @returns
     */
    get<T>(url: string, options: unknown): Observable<T> {
        console.log('Переопределенный http client');
        return super.get<T>(url, options);
    }
}
