import { LoadingController } from '@ionic/angular';
import { LoadingOptions } from '@ionic/core';
import { Observable, of } from 'rxjs';
import { finalize, switchMap } from 'rxjs/operators';
import { AppModule } from 'src/app/app.module';

/**
 * Декоратор загрузчика
 * Использовать для декорирования методов, возвращающих результат
 * выполнения http-запроса (одного или нескольких, объединенных одним методом)
 * На время выполнения метода экран будет заблокирован загрузчиком
 * @param opts
 * @returns
 */
export function loading(opts?: LoadingOptions) {
    // tslint:disable: only-arrow-functions
    return function(target: any, methodName: string, descriptor?: PropertyDescriptor) {
        const loadingController = AppModule.injector.get(LoadingController);
        const originalFunction = descriptor.value;

        descriptor.value = function(...args: any[]) {
            const loader$: Observable<HTMLIonLoadingElement> = new Observable(subscriber => {
                loadingController.create(opts)
                    .then(loader => {
                        subscriber.next(loader);
                        subscriber.complete();
                    })
                    .catch(err => {
                        subscriber.next(err);
                        subscriber.complete();
                    });
            });
            return loader$.pipe(
                switchMap(l => {
                    l.present();
                    const result = originalFunction.apply(this, args) as Observable<any>;
                    if (!(result instanceof Observable)) {
                        l.dismiss();
                        throw Error('Декоратор @loading() предназначен только для методов, возвращающих Observable');
                    }
                    return result.pipe(
                        finalize(() => l.dismiss())
                    );
                }),
            );
        };
        return descriptor;
    };
}
