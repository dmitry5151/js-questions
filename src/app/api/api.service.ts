import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

import { loading } from '../shared/decorators/loading.decorator';

@Injectable({
    providedIn: 'root'
})
export class ApiService {

    constructor(
        private http: HttpClient,
    ) { }

    @loading({
        message: 'Загрузка...',
        duration: 10000
    })
    getQuestions(theme: string): Observable<any> {
        return this.http.get(`https://jsquestions-6997f.firebaseio.com/${theme.toLowerCase()}.json`);
    }
}
