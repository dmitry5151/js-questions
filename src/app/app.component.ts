import { Component, OnDestroy, OnInit } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
@Component({
    selector: 'app-root',
    templateUrl: 'app.component.html',
    styleUrls: ['app.component.scss'],
})
export class AppComponent implements OnInit, OnDestroy {
    public appPages = [
        { title: 'Javascript', url: '/folder/Js', icon: 'fab fa-js-square' },
        { title: 'Angular', url: '/folder/Angular', icon: 'fab fa-angular' },
        // { title: 'Favorites', url: '/folder/Favorites', icon: 'heart' },
        // { title: 'Archived', url: '/folder/Archived', icon: 'archive' },
        // { title: 'Trash', url: '/folder/Trash', icon: 'trash' },
        // { title: 'Spam', url: '/folder/Spam', icon: 'warning' },
    ];
    public labels = ['Family', 'Friends', 'Notes', 'Work', 'Travel', 'Reminders'];
    constructor(
        // private firestore: AngularFirestore,
    ) { }

    ngOnInit(): void {
        //  this.firestore.collection('questions-test').get();
    }

    ngOnDestroy(): void { }
}
