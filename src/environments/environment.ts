// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

// <!--The core Firebase JS SDK is always required and must be listed first-- >
//   <script src="https://www.gstatic.com/firebasejs/8.3.2/firebase-app.js" > </script>

//     < !--TODO: Add SDKs for Firebase products that you want to use
// https://firebase.google.com/docs/web/setup#available-libraries -->

// <script>
//   // Your web app's Firebase configuration
//   var firebaseConfig = {
//   apiKey: "AIzaSyCQFNYdtOPC8NM8d9kgcKeaQ8JQLWFcwg0",
//   authDomain: "web-developer-interview.firebaseapp.com",
//   projectId: "web-developer-interview",
//   storageBucket: "web-developer-interview.appspot.com",
//   messagingSenderId: "1034254914846",
//   appId: "1:1034254914846:web:909c98ddef3e322868a50f"
// };
// // Initialize Firebase
// firebase.initializeApp(firebaseConfig);
// </script>

export const environment = {
  production: false,
  firebaseConfig: {
    apiKey: 'AIzaSyCQFNYdtOPC8NM8d9kgcKeaQ8JQLWFcwg0',
    authDomain: 'web-developer-interview.firebaseapp.com',
    projectId: 'web-developer-interview',
    storageBucket: 'web-developer-interview.appspot.com',
    messagingSenderId: '1034254914846',
    appId: '1:1034254914846:web:909c98ddef3e322868a50f',
  },
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
