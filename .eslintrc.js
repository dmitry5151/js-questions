module.exports = {
    root: true,
    env: {
        browser: true,
        es2021: true,
        node: true
    },
    overrides: [
        {
            files: ["*.ts"],
            parserOptions: {
                project: [
                    "tsconfig.*?.json",
                    "e2e/tsconfig.json"
                ],
                createDefaultProgram: true
            },
            extends: ["plugin:@ionic/eslint-config/recommended"],
            rules: {
                "@typescript-eslint/no-var-requires": "off",
                "@typescript-eslint/no-empty-function": "off",
            }
        },
        {
            files: ["*.component.html"],
            extends: ["plugin:@angular-eslint/template/recommended"],
            rules: {
                "max-len": ["error", { "code": 140 }]
            }
        },
        {
            files: ["*.component.ts"],
            extends: ["plugin:@angular-eslint/template/process-inline-templates"]
        }
    ]
};
